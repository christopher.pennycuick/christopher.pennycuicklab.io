function makeTabs(menuSelector, contentSelector, defaultRef, errorRef) {
 
	var tab_anchors = document.querySelectorAll(menuSelector);
	var contents = document.querySelectorAll(contentSelector);

	function selectTab(ref) {
		if (!ref || ref[0] !== '#') {
			ref = defaultRef;
		}
	
		var found = false;
		for (var i = 0; i < tab_anchors.length; i++) {
			if (tab_anchors[i].getAttribute('href') === ref) {
				found = true;
				tab_anchors[i].classList.add('active');
			} else {
				tab_anchors[i].classList.remove("active");
			}
		}

		if (!found) {
			ref = errorRef;
		}

		for (var i = 0; i < contents.length; i++) {
			if (('#'+contents[i].id) === ref) {
				contents[i].classList.add('active');
			} else {
				contents[i].classList.remove("active");
			}
		}
	}
 
	window.onpopstate = function () {
		selectTab(document.location.hash || null);
	};
 
	selectTab(document.location.hash || null);
}
